package stepdefs;

import controls.MenuControl;
import io.cucumber.java.en.When;

public class GeneralSteps {

    @When("^hover over menu (Anasayfa|Raporlar|AracRota|AracVideoFotograf|YapayZeka|GüzergahIslemleri|AlarmIslemleri|RotaIslemleri|PersinelIslemleri|ReklamAnonsVideo|MüsteriIslemleri|YönetimVeAyarlar)$")
    public void hoverOverElementWithAttribute(String menu){
        switch (menu){
            case "Anasayfa":
                MenuControl.fromLabel("Anasayfa ( filo_admin ) ").hover();
                break;
            case "Raporlar":
                MenuControl.fromLabel("Raporlar").hover();
                break;
            case "AracRota":
                MenuControl.fromLabel("Araç Rota").hover();
                break;
            case "AracVideoFotograf":
                MenuControl.fromLabel("Araç Video-Fotoğraf").hover();
                break;
            case "YapayZeka":
                MenuControl.fromLabel("Yapay Zeka").hover();
                break;
            case "GüzergahIslemleri":
                MenuControl.fromLabel("Güzergah İşlemleri").hover();
                break;
            case "AlarmIslemleri":
                MenuControl.fromLabel("Alarm İşlemleri").hover();
                break;
            case "RotaIslemleri":
                MenuControl.fromLabel("Rota İşlemleri").hover();
                break;
            case "PersinelIslemleri":
                MenuControl.fromLabel("Personel İşlemleri").hover();
                break;
            case "ReklamAnonsVideo":
                MenuControl.fromLabel("Reklam-Anons-Video").hover();
                break;
            case "MüsteriIslemleri":
                MenuControl.fromLabel("Müşteri İşlemleri").hover();
                break;
            case "YönetimVeAyarlar":
                MenuControl.fromLabel("Yönetim ve Ayarlar").hover();
                break;
        }
    }
}

